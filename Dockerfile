FROM golang:1.15.2-buster as build

RUN mkdir -p /build
RUN apt install -y git make
RUN go get -d github.com/siddontang/go-mysql-elasticsearch; chmod 777 -R /go/src/github.com/siddontang/go-mysql-elasticsearch && cd /go/src/github.com/siddontang/go-mysql-elasticsearch/ && make && cp /go/src/github.com/siddontang/go-mysql-elasticsearch/bin/go-mysql-elasticsearch /build/

FROM mysql:8.0.21
RUN mkdir -p /app/etc/ && touch /app/etc/river.toml
WORKDIR /app/
COPY --from=build /build .
ENTRYPOINT []
CMD ["/app/go-mysql-elasticsearch","-config=/app/etc/river.toml"]
